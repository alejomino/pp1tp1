CREATE DATABASE `agenda`;
USE agenda;
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `TipoContacto` varchar(45) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Email` varchar(45) NOT NULL,  
  `Pais` varchar(45),
  `Provincia` varchar(45),
  `Calle` varchar(45),
  `Altura` varchar(45),
  `Localidad` varchar(45),
  `CodigoPostal` varchar(45),
  `Piso` varchar(45),
  `Depto` varchar(45),
  `Nacimiento` varchar(45) NOT NULL,
  `DestinoPreferido` varchar(45) NOT NULL,
  PRIMARY KEY (`idPersona`)
);
