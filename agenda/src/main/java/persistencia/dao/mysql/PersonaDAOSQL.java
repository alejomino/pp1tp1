package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO {
	private static final String insert = "INSERT INTO personas(idPersona, tipocontacto, nombre, apellido, telefono, email, pais, provincia, "
			+ "calle, altura, localidad,codigopostal, piso, depto, nacimiento,destinopreferido) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
	private static final String edit = "UPDATE personas SET idPersona = ?, tipocontacto = ?, nombre = ?, apellido = ?, telefono = ?, email = ?, pais = ?, provincia = ?,"
			+ " calle = ?, altura = ?, localidad = ?, codigopostal = ?, piso = ?, depto = ?, nacimiento = ?, destinopreferido = ? WHERE idPersona = ? ";

	public boolean edit(PersonaDTO persona_a_editar, PersonaDTO persona_editada) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditExitoso = false;
		try {
			statement = conexion.prepareStatement(edit);
			statement.setInt(1, persona_a_editar.getIdPersona());
			statement.setString(2, persona_editada.getTipoContacto());
			statement.setString(3, persona_editada.getNombre());
			statement.setString(4, persona_editada.getApellido());
			statement.setString(5, persona_editada.getTelefono());
			statement.setString(6, persona_editada.getEmail());
			statement.setString(7, persona_editada.getPais());
			statement.setString(8, persona_editada.getProvincia());
			statement.setString(9, persona_editada.getCalle());
			statement.setString(10, persona_editada.getAltura());
			statement.setString(11, persona_editada.getLocalidad());
			statement.setString(12, persona_editada.getCod_postal());
			statement.setString(13, persona_editada.getPiso());
			statement.setString(14, persona_editada.getDepto());
			statement.setString(15, persona_editada.getFecha_nac());
			statement.setString(16, persona_editada.getDestino_pref());
			statement.setInt(17, persona_a_editar.getIdPersona());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isEditExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isEditExitoso;

	}

	public boolean insert(PersonaDTO persona) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getTipoContacto());
			statement.setString(3, persona.getNombre());
			statement.setString(4, persona.getApellido());
			statement.setString(5, persona.getTelefono());
			statement.setString(6, persona.getEmail());
			statement.setString(7, persona.getPais());
			statement.setString(8, persona.getProvincia());
			statement.setString(9, persona.getCalle());
			statement.setString(10, persona.getAltura());
			statement.setString(11, persona.getLocalidad());
			statement.setString(12, persona.getCod_postal());
			statement.setString(13, persona.getPiso());
			statement.setString(14, persona.getDepto());
			statement.setString(15, persona.getFecha_nac());
			statement.setString(16, persona.getDestino_pref());
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return isInsertExitoso;
	}

	public boolean delete(PersonaDTO persona_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if (statement.executeUpdate() > 0) {
				conexion.commit();
				isdeleteExitoso = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	public List<PersonaDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; // Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				personas.add(getPersonaDTO(resultSet));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return personas;
	}

	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("idPersona");
		String tipoContacto = resultSet.getString("TipoContacto");
		String nombre = resultSet.getString("Nombre");
		String apellido = resultSet.getString("Apellido");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		String pais = resultSet.getString("Pais");
		String provincia = resultSet.getString("Provincia");
		String calle = resultSet.getString("Calle");
		String altura = resultSet.getString("Altura");
		String localidad = resultSet.getString("Localidad");
		String cod_postal = resultSet.getString("CodigoPostal");
		String piso = resultSet.getString("Piso");
		String depto = resultSet.getString("Depto");
		String fecha_nac = resultSet.getString("Nacimiento");
		String destino_pref = resultSet.getString("DestinoPreferido");
		return new PersonaDTO(id, tipoContacto, nombre, apellido, tel, email, pais, provincia, calle, altura, localidad,
				cod_postal, piso, depto, fecha_nac, destino_pref);
	}

}
