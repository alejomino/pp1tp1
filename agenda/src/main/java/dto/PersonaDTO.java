package dto;

public class PersonaDTO {
	private int idPersona;
	private String tipoContacto;

	private String nombre;
	private String apellido;
	private String telefono;
	private String email;
	private String pais;
	private String provincia;
	private String calle;
	private String altura;
	private String localidad;
	private String cod_postal;
	private String piso;
	private String depto;
	private String fecha_nac;
	private String destino_pref;

	public PersonaDTO(int idPersona, String tipoContacto, String nombre, String apellido, String telefono, String email,
			String pais, String provincia, String calle, String altura, String localidad, String cod_postal,
			String piso, String depto, String fecha_nac, String destino_pref) {
		this.idPersona = idPersona;
		this.tipoContacto = tipoContacto;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.email = email;
		this.pais = pais;
		this.provincia = provincia;
		this.calle = calle;
		this.altura = altura;
		this.localidad = localidad;
		this.cod_postal = cod_postal;
		this.piso = piso;
		this.depto = depto;
		this.fecha_nac = fecha_nac;
		this.destino_pref = destino_pref;
	}

	public String getCod_postal() {
		return cod_postal;
	}

	public void setCod_postal(String cod_postal) {
		this.cod_postal = cod_postal;
	}

	public String getDestino_pref() {
		return destino_pref;
	}

	public void setDestino_pref(String destino_pref) {
		this.destino_pref = destino_pref;
	}

	public int getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public String getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(String fecha_nac) {
		this.fecha_nac = fecha_nac;
	}

}
