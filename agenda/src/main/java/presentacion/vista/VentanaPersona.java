package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;

import com.toedter.calendar.JDateChooser;

import java.text.SimpleDateFormat;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaPersona extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	private JButton btnAgregarPersona;
	private static VentanaPersona INSTANCE;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtTelefono;
	private JTextField txtDom_calle;
	private JTextField txtDom_altura;
	private JTextField txtDom_piso;
	private JTextField txtDom_depto;
	private JTextField txtEmail;
	private JDateChooser dateChooser;
	private String fecha_nac;
	private String paisSeleccionado;
	private String provSeleccionada;
	private String tipoContactoSeleccionado;
	private String localidadSeleccionada;
	private JTextField txtCodPostal;
	private JTextField txtDestinoPref;
	private JComboBox<String> listaDesplegableContacto;
	private JComboBox<String> listaDesplegablePais;
	private JComboBox<String> listaDesplegableProvincia;
	private JComboBox<String> listaDesplegableLocalidad;

	public static VentanaPersona getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaPersona();
			return new VentanaPersona();
		} else
			return INSTANCE;
	}

	private VentanaPersona() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 406, 499);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 372, 405);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 13, 96, 14);
		panel.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(196, 13, 98, 14);
		panel.add(lblApellido);

		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(191, 42, 62, 14);
		panel.add(lblTelefono);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(26, 228, 51, 14);
		panel.add(lblEmail);

		JLabel lblDomicilio = new JLabel("Domicilio:");
		lblDomicilio.setBounds(10, 70, 113, 14);
		panel.add(lblDomicilio);

		JLabel lblDomicilio_pais = new JLabel("Pa\u00EDs");
		lblDomicilio_pais.setBounds(26, 100, 51, 14);
		panel.add(lblDomicilio_pais);

		JLabel lblDomicilio_provincia = new JLabel("Provincia");
		lblDomicilio_provincia.setVisible(false);
		lblDomicilio_provincia.setBounds(194, 100, 69, 14);
		panel.add(lblDomicilio_provincia);

		JLabel lblDomicilio_calle = new JLabel("Calle");
		lblDomicilio_calle.setBounds(26, 132, 34, 14);
		panel.add(lblDomicilio_calle);

		JLabel lblDomicilio_altura = new JLabel("Altura");
		lblDomicilio_altura.setBounds(204, 132, 34, 14);
		panel.add(lblDomicilio_altura);

		JLabel lblDomicilio_piso = new JLabel("Piso (opc.)");
		lblDomicilio_piso.setBounds(26, 196, 76, 14);
		panel.add(lblDomicilio_piso);

		JLabel lblDomicilio_depto = new JLabel("Depto (opc.)");
		lblDomicilio_depto.setBounds(194, 194, 86, 14);
		panel.add(lblDomicilio_depto);

		JLabel lblDomicilio_localidad = new JLabel("Localidad");
		lblDomicilio_localidad.setBounds(26, 164, 69, 14);
		lblDomicilio_localidad.setVisible(false);
		panel.add(lblDomicilio_localidad);

		JLabel lblFecha_nac = new JLabel("Fecha de cumplea\u00F1os");
		lblFecha_nac.setBounds(26, 260, 149, 14);
		panel.add(lblFecha_nac);

		JLabel lblCodPostal = new JLabel("Cod. Postal");
		lblCodPostal.setBounds(196, 164, 61, 14);
		lblCodPostal.setVisible(false);
		panel.add(lblCodPostal);

		JLabel lblDestinoPref = new JLabel("Destino Preferido");
		lblDestinoPref.setBounds(26, 316, 118, 14);
		panel.add(lblDestinoPref);

		txtNombre = new JTextField();
		txtNombre.setBounds(72, 10, 98, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(255, 10, 98, 20);
		panel.add(txtApellido);

		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(90, 224, 270, 20);
		panel.add(txtEmail);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(255, 39, 98, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);

		txtDom_calle = new JTextField();
		txtDom_calle.setColumns(10);
		txtDom_calle.setBounds(90, 128, 85, 20);
		panel.add(txtDom_calle);

		txtDom_altura = new JTextField();
		txtDom_altura.setColumns(10);
		txtDom_altura.setBounds(248, 129, 93, 20);
		panel.add(txtDom_altura);

		txtDom_piso = new JTextField();
		txtDom_piso.setColumns(10);
		txtDom_piso.setBounds(90, 192, 93, 20);
		panel.add(txtDom_piso);

		txtDom_depto = new JTextField();
		txtDom_depto.setColumns(10);
		txtDom_depto.setBounds(274, 193, 86, 20);
		panel.add(txtDom_depto);

		txtCodPostal = new JTextField();
		txtCodPostal.setEditable(false);
		txtCodPostal.setBounds(267, 161, 86, 20);
		txtCodPostal.setVisible(false);
		panel.add(txtCodPostal);
		txtCodPostal.setColumns(10);

		txtDestinoPref = new JTextField();
		txtDestinoPref.setBounds(140, 313, 86, 20);
		panel.add(txtDestinoPref);
		txtDestinoPref.setColumns(10);

		String[] paises = new String[] { "", "Argentina", "Brasil", "Bolivia", "Chile", "Uruguay", "Peru", "Paraguay",
				"Venezuela", "Mexico" };
		listaDesplegablePais = new JComboBox<>();
		for (String pa : paises) {
			listaDesplegablePais.addItem(pa);
		}
		listaDesplegablePais.setBounds(90, 96, 86, 22);
		panel.add(listaDesplegablePais);

		String[] provincias = new String[] { "", "Buenos Aires", "Ciudad Autonoma de Buenos Aires", "Catamarca",
				"Chaco", "Chubut", "Cordoba", "Corrientes", "Entre Rios", "Formosa", "Jujuy", "La Pampa", "La Rioja",
				"Mendoza", "Misiones", "Neuquen", "Rio Negro", "Salta", "San Juan", "San Luis", "Santa Cruz",
				"Santa Fe", "Santiago del Estero", "Tierra del Fuego", "Tucuman" };
		;

		listaDesplegableProvincia = new JComboBox<>();
		listaDesplegableProvincia.setBounds(255, 96, 105, 22);
		listaDesplegableProvincia.setVisible(false);
		listaDesplegableProvincia.addItem("");

		panel.add(listaDesplegableProvincia);

		listaDesplegablePais.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				Object paisSelec = listaDesplegablePais.getSelectedItem();

				if ("Argentina".equals(paisSelec)) {
					listaDesplegableProvincia.removeAllItems();
					lblDomicilio_provincia.setVisible(true);
					listaDesplegableProvincia.setVisible(true);
					for (String p : provincias) {
						listaDesplegableProvincia.addItem(p);
					}
					listaDesplegableProvincia.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent event) {
							Object provSelec = listaDesplegableProvincia.getSelectedItem();
							if ("Buenos Aires".equals(provSelec)) {
								lblDomicilio_localidad.setVisible(true);
								lblCodPostal.setVisible(true);
								listaDesplegableLocalidad.setVisible(true);
								txtCodPostal.setVisible(true);

							} else {
								lblDomicilio_localidad.setVisible(false);
								lblCodPostal.setVisible(false);
								listaDesplegableLocalidad.setVisible(false);
								txtCodPostal.setVisible(false);
								localidadSeleccionada = "Sin informacion";
								txtCodPostal.setText("Sin informacion");
							}

							if (provSelec != null) {
								provSeleccionada = provSelec.toString();
							} else {
								provSeleccionada = "";
							}

							paisSeleccionado = paisSelec.toString();
						}
					});
				} else {
					lblDomicilio_provincia.setVisible(false);
					lblDomicilio_localidad.setVisible(false);
					lblCodPostal.setVisible(false);
					listaDesplegableProvincia.setVisible(false);
					listaDesplegableLocalidad.setVisible(false);
					txtCodPostal.setVisible(false);
					provSeleccionada = "";
					paisSeleccionado = paisSelec.toString();
					localidadSeleccionada = "Sin informacion";
					txtCodPostal.setText("Sin informacion");
				}

			}
		});

		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("d/MM/yyyy");
		dateChooser.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					fecha_nac = sdf.format(dateChooser.getDate());
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Elegi fecha");
				}
			}
		});

		dateChooser.setBounds(156, 256, 149, 22);
		panel.add(dateChooser);

		String[] tiposContacto = new String[] { "Tipo de Contacto", "Amigo", "Familiar", "Trabajo", "Otro" };
		listaDesplegableContacto = new JComboBox<>();
		for (String co : tiposContacto) {
			listaDesplegableContacto.addItem(co);
		}
		listaDesplegableContacto.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				Object contactoSelec = listaDesplegableContacto.getSelectedItem();
				if (contactoSelec.equals("Tipo de Contacto")) {
					JOptionPane.showMessageDialog(null, "Elegi tipo de contacto");
				} else {
					tipoContactoSeleccionado = contactoSelec.toString();
				}

			}
		});

		listaDesplegableContacto.setBounds(26, 38, 124, 22);
		panel.add(listaDesplegableContacto);

		String[] tiposLocalidades = new String[] { "", "Acassuso", "Almirante Brown", "Alto Los Cardales", "Avellaneda",
				"B General S Martin", "Barrio El Cazador", "Beccar", "Bella Vista", "Billinghurst / San martin / Maipu",
				"Bosch", "Boulogne", "Campana", "Campo De Mayo", "Carapachay", "Castelar", "Castelar", "Ciudad Evita",
				"Ciudad Jardin El Palomar", "Ciudad Madero", "Ciudadela", "Cuartel V", "Del Viso", "Dique Lujan",
				"Don Torcuato", "Don Torcuato", "El Palomar", "El Talar", "Escobar", "Estacion Moreno",
				"Estacion Moreno", "Fatima / Manzone / Astolfi", "Florida", "Florida Oeste", "Francisco Alvarez",
				"General Las Heras", "General Pacheco", "General Rodriguez", "General Rodriguez", "General Rodriguez",
				"General Rodriguez", "Grand Bourg", "Haedo", "Hurlingham / William Morris",
				"Ingeniero Adolfo Sourdeaux", "Ingeniero Maschwitz", "Islas", "Ituzaingo", "Jose Clemente Paz",
				"Jose Clemente Paz", "Jose Clemente Paz", "Jose Hernandez", "Jose Leon Suarez", "La Lonja", "La Lucia",
				"La Lucila / Olivo ", "La Reja", "La Reja", "Loma Verde", "Loma Verde", "Los Polvorines",
				"Lomas Del Mirador", "Los Cardales", "Lujan", "Lujan", "Malvinas Argentinas", "Malvinas Argentinas",
				"Malvinas Argentinas / pablo noguez ", "Manuel Alberti", "Manzanares", "Manzanares",
				"Maquinista F Savio", "Martin Coronado", "Martin Coronado / Bosch", "Martinez", "Martinez",
				"Matheu / Zelaya", "Moreno", "Moreno", "Munro", "Muñiz", "Moron", "Nordelta Tigre", "Olivos",
				"Open Door", "Pablo Podesta", "Pablo Podesta", "Paso Del Rey", "Pilar", "Pilar", "Pilar",
				"Ricardo Rojas", "Ramos Mejia", "San Andres", "San Andres", "San Andres", "San Fernando", "San Isidro",
				"San Martin", "San Miguel", "Tigre", "Tigre", "Tortuguitas", "Tres De Febrero", "Troncos Del Talar",
				"Trujui", "Victoria", "Villa Adelina", "Villa Astolfi", "Villa Ballester", "Villa Brown",
				"Villa De Mayo", "Villa Gobernador Udaondo", "Villa Lynch", "Villa Martelli", "Villa Raffo",
				"Villa Ramallo", "Villa Ramallo", "Villa Roch", "Villa Rosa", "Virrey Del Pino", "Virrey Del Pino",
				"Virreyes", "Zelaya", "Zarate", "Sin informacion" };
		
		String[] codigosPostales = new String[] { "", "1641", "1846", "2816", "1870", "6000", "1626", "1643", "1661",
				"1650", "7621", "1609", "2804", "1659", "1606", "1710", "1712", "1778", "1685", "1768", "1702", "1740",
				"1669", "1622", "1611", "2610", "1684", "1618", "1625", "1901", "1935", "1633", "1602", "1604", "1746",
				"1741", "1617", "1717", "1747", "1748", "1749", "1615", "1706", "1686", "1612", "1623", "6667", "1714",
				"1660", "1665", "1666", "1903", "1655", "8200", "7603", "1637", "1738", "1745", "1628", "1981", "1613",
				"1752", "2814", "6700", "6702", "1851", "1916", "1616", "1664", "1632", "2025", "1620", "1683", "1682",
				"1639", "1640", "1627", "1743", "1744", "1605", "1662", "1708", "1670", "1636", "6708", "1657", "1687",
				"1742", "1629", "1630", "1634", "1610", "1704", "1651", "6354", "8180", "1646", "1642", "1654", "1663",
				"1648", "1649", "1667", "1652", "1608", "1736", "1644", "1607", "2032", "1653", "1892", "1614", "1713",
				"1672", "1603", "1675", "2914", "2915", "7101", "1631", "1763", "1764", "1645", "2020", "2800",
				"Sin informacion" };
		
		listaDesplegableLocalidad = new JComboBox<>();
		for (String lo : tiposLocalidades) {
			listaDesplegableLocalidad.addItem(lo);
		}
		listaDesplegableLocalidad.setBounds(90, 160, 85, 22);
		listaDesplegableLocalidad.setVisible(false);
		panel.add(listaDesplegableLocalidad);
		listaDesplegableLocalidad.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				Object localidadSelec = listaDesplegableLocalidad.getSelectedItem();
				Integer localidadIndex = listaDesplegableLocalidad.getSelectedIndex();
				txtCodPostal.setText(codigosPostales[localidadIndex]);
				localidadSeleccionada = localidadSelec.toString();
			}
		});

		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		btnAgregarPersona.setBounds(282, 427, 89, 23);
		contentPane.add(btnAgregarPersona);

		this.setVisible(false);
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtTelefono() {
		return txtTelefono;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public JTextField getTxtDom_calle() {
		return txtDom_calle;
	}

	public JTextField getTxtDom_altura() {
		return txtDom_altura;
	}

	public String getTxtLocalidad() {
		return localidadSeleccionada;
	}

	public JTextField getTxtCodPostal() {
		return txtCodPostal;
	}

	public JTextField getTxtDom_piso() {
		return txtDom_piso;
	}

	public JTextField getTxtDom_depto() {
		return txtDom_depto;
	}

	public String getPaisSeleccionado() {
		return paisSeleccionado;
	}

	public String getTipoContacto() {
		return tipoContactoSeleccionado;
	}

	public String getProvSeleccionada() {
		return provSeleccionada;
	}

	public String getFecha_nac() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		fecha_nac = sdf.format(dateChooser.getDate());
		return fecha_nac;
	}

	public JTextField getDestinoPref() {
		return txtDestinoPref;
	}

	public JButton getBtnAgregarPersona() {
		return btnAgregarPersona;

	}

	public void cerrar() {

		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.txtTelefono.setText(null);
		this.txtEmail.setText(null);
		this.txtDom_altura.setText(null);
		this.txtDom_calle.setText(null);
		this.txtDom_piso.setText(null);
		this.txtDom_depto.setText(null);

		this.fecha_nac = (null);
		this.txtDestinoPref.setText(null);
		;
		this.txtCodPostal.setText(null);

		this.dispose();
	}
}
