package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;

public class Vista {
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnEditar;
	private JButton btnReporteCodigoPostal;
	private DefaultTableModel modelPersonas;
	private JButton btnReporteCodigoPostalZtoA;
	private String[] nombreColumnas = { "Tipo de Contacto", "Nombre", "Apellido", "Telefono", "Email", "Pais",
			"Provincia", "Calle", "Altura", "Localidad", "Codigo Postal", "Piso", "Depto", "Fecha nac",
			"Destino Preferido" };

	public Vista() {
		super();
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1170, 326);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1154, 283);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 997, 210);
		panel.add(spPersonas);

		modelPersonas = new DefaultTableModel(null, nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);

		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);

		spPersonas.setViewportView(tablaPersonas);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAgregar.setBounds(36, 232, 140, 44);
		panel.add(btnAgregar);

		btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnEditar.setBounds(435, 232, 140, 44);
		panel.add(btnEditar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnBorrar.setBounds(847, 232, 140, 44);
		panel.add(btnBorrar);

		btnReporte = new JButton("Destino Preferido");
		btnReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnReporte.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnReporte.setBounds(1017, 60, 127, 44);
		panel.add(btnReporte);

		btnReporteCodigoPostal = new JButton("CP Ascendente");
		btnReporteCodigoPostal.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnReporteCodigoPostal.setBounds(1017, 115, 127, 44);
		panel.add(btnReporteCodigoPostal);

		btnReporteCodigoPostalZtoA = new JButton("CP Descendente");
		btnReporteCodigoPostalZtoA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnReporteCodigoPostalZtoA.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnReporteCodigoPostalZtoA.setBounds(1017, 170, 127, 44);
		panel.add(btnReporteCodigoPostalZtoA);

		JLabel lblReporteDestino = new JLabel("Reportes");
		lblReporteDestino.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblReporteDestino.setBounds(1046, 35, 85, 14);
		panel.add(lblReporteDestino);
	}

	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(null, "¿Estas seguro que quieres salir de la Agenda?",
						"Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
				if (confirm == 0) {
					Conexion.getConexion().cerrarConexion();
					System.exit(0);
				}
			}
		});
		this.frame.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public JButton getBtnReporteCodigoPostal() {
		return btnReporteCodigoPostal;
	}

	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}

	public JTable getTablaPersonas() {
		return tablaPersonas;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); // Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla) {
			String tipoContacto = p.getTipoContacto();
			String nombre = p.getNombre();
			String apellido = p.getApellido();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String pais = p.getPais();
			String provincia = p.getProvincia();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String localidad = p.getLocalidad();
			String cod_postal = p.getCod_postal();
			String piso = p.getPiso();
			String depto = p.getDepto();
			String fecha_nac = p.getFecha_nac();
			String destino_pref = p.getDestino_pref();
			Object[] fila = { tipoContacto, nombre, apellido, tel, email, pais, provincia, calle, altura, localidad,
					cod_postal, piso, depto, fecha_nac, destino_pref };
			this.getModelPersonas().addRow(fila);

		}

	}

	public JButton getBtnReporteCodigoPostalZtoA() {
		// TODO Auto-generated method stub
		return btnReporteCodigoPostalZtoA;
	}
}
