package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaEditar;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import dto.PersonaDTO;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private VentanaPersona ventanaPersona;
	private Agenda agenda;
	private VentanaEditar ventanaEditar;

	public Controlador(Vista vista, Agenda agenda) {
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r, "ReporteAgenda.jasper"));
		this.vista.getBtnReporteCodigoPostal()
				.addActionListener(r -> mostrarReporte(r, "ReporteAgendaCodigoPostal.jasper"));
		this.vista.getBtnReporteCodigoPostalZtoA()
				.addActionListener(r -> mostrarReporte(r, "ReporteAgendaCodigoPostalZtoA.jasper"));
		this.vista.getBtnEditar().addActionListener(e -> ventanaEditarPersona(e));
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaEditar = VentanaEditar.getInstance();
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));
		this.ventanaEditar.getBtnAgregarPersona().addActionListener(j -> editarPersona(j));
		this.agenda = agenda;
	}

	private void ventanaEditarPersona(ActionEvent j) {
		this.ventanaEditar.mostrarVentana();
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		PersonaDTO iterable;
		for (int fila : filasSeleccionadas) {
			iterable = this.personasEnTabla.get(fila);
			this.ventanaEditar.cargarDatosPrevios(iterable);
		}
	}

	private void editarPersona(ActionEvent e) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			String tipoContacto = this.ventanaEditar.getTipoContacto();
			String nombre = this.ventanaEditar.getTxtNombre().getText();
			String apellido = this.ventanaEditar.getTxtApellido().getText();
			String tel = ventanaEditar.getTxtTelefono().getText();
			String email = ventanaEditar.getTxtEmail().getText();
			String pais = ventanaEditar.getPaisSeleccionado();
			String provincia = ventanaEditar.getProvSeleccionada();
			String calle = ventanaEditar.getTxtDom_calle().getText();
			String altura = ventanaEditar.getTxtDom_altura().getText();
			String localidad = ventanaEditar.getTxtLocalidad();
			String cod_postal = ventanaEditar.getTxtCodPostal().getText();
			String piso = ventanaEditar.getTxtDom_piso().getText();
			String depto = ventanaEditar.getTxtDom_depto().getText();
			String fecha_nac = ventanaEditar.getFecha_nac();
			String destino_pref = ventanaEditar.getDestinoPref().getText();
			PersonaDTO persona_editada = new PersonaDTO(0, tipoContacto, nombre, apellido, tel, email, pais, provincia,
					calle, altura, localidad, cod_postal, piso, depto, fecha_nac, destino_pref);
			this.agenda.editarPersona(this.personasEnTabla.get(fila), persona_editada);
		}

		this.refrescarTabla();
		this.ventanaEditar.cerrar();
	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.mostrarVentana();
	}

	private void guardarPersona(ActionEvent p) {
		String tipoContacto = ventanaPersona.getTipoContacto();
		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String apellido = this.ventanaPersona.getTxtApellido().getText();
		String tel = ventanaPersona.getTxtTelefono().getText();
		String email = ventanaPersona.getTxtEmail().getText();
		String pais = ventanaPersona.getPaisSeleccionado();
		String provincia = ventanaPersona.getProvSeleccionada();
		String calle = ventanaPersona.getTxtDom_calle().getText();
		String altura = ventanaPersona.getTxtDom_altura().getText();
		String localidad = ventanaPersona.getTxtLocalidad();
		String cod_postal = ventanaPersona.getTxtCodPostal().getText();
		String piso = ventanaPersona.getTxtDom_piso().getText();
		String depto = ventanaPersona.getTxtDom_depto().getText();
		String fecha_nac = ventanaPersona.getFecha_nac();
		String destino_pref = ventanaPersona.getDestinoPref().getText();
		PersonaDTO nuevaPersona = new PersonaDTO(0, tipoContacto, nombre, apellido, tel, email, pais, provincia, calle,
				altura, localidad, cod_postal, piso, depto, fecha_nac, destino_pref);
		this.agenda.agregarPersona(nuevaPersona);
		this.refrescarTabla();
		this.ventanaPersona.cerrar();
	}

	private void mostrarReporte(ActionEvent r, String nombreArchivo) {
		ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas(), nombreArchivo);
		reporte.mostrar();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
		}

		this.refrescarTabla();
	}

	public void inicializar() {
		this.refrescarTabla();
		this.vista.show();
	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
